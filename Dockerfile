FROM php:8.2.6-fpm

RUN apt-get update -y \
    && apt-get install -y nginx libmcrypt-dev \
    && pecl update-channels \
    && pecl install mcrypt 
    
RUN docker-php-ext-enable mcrypt

COPY nginx-site.conf /etc/nginx/sites-enabled/default
COPY entrypoint.sh /etc/entrypoint.sh
RUN chmod +x /etc/entrypoint.sh

COPY --chown=www-data:www-data ./www/ /var/www/mysite

WORKDIR /var/www/mysite

EXPOSE 80 443

ENTRYPOINT ["/etc/entrypoint.sh"]    
